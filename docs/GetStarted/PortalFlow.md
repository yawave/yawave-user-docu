# Portal Workflows

Introduce content in this article.

Insert a workflow chart

## Create a Portal
Describe the following as high level steps:

- Create a portal
- Add publications/components
- How to deploy a portal

Provide links to relevant topics in the documentation.
