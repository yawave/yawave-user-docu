# Publication Workflows

Publications are at the heart of your content management and engagement strategy. 

Yawave provides a variety of content builder, engagement and administration tools that help you assemble your publication with the level of complexity and audience engagement you require. We explore these tools in depth throughout our documentation. 

To get started now, you can follow the basic publication workflow shown below:

![Flowchart](../assets/images/flowchart.jpg)

## Create a Publicaton

1. On the yawave home page, click **Discover > Create Publication**.
    ![Create Publication](../assets/images/Create_pub.jpg)
2. Select your publication type, for example **Article** or **Landing Page**.
3. [Use the Publication Builder](../Publish/PubBuilder/AboutPubBuild.md) to design and create your publication. Select the content types you want to include, and configure their content settings.
4. Save your publication as a draft. 

For more information about publications, see [About Publications](../Publish/Publications/Overview.md).

## Set Up Engagement Tools and Categories

1. From the yawave task menu, select **Administrate**.
2. Click **Tools > Engage** and [add engagement tools](../Administrate/Tools/Engage.md), such as the Share tool. Tools allow your visitors to share content, make payments, provide feedback, and more. You can then collect and analyze site data.![Engage Tool](../assets/images/engage_tools.jpg)
 1. Click **Classify > Catagories** and [create the appropriate category](../Administrate/Classifications/Categories.md) for your publication, specifying the type of publication, such as business, news, or sports.    ![Category](../assets/images/create_cat.jpg)
3. Return to the draft of your new publication and apply the engagement tools and categories you created.

For more information, see:

- [About Engagement Tools](../Administrate/Tools/About-Tools.md)
- [About Categories, Attributes, and Tags](../Administrate/Classifications/About-Classifications.md)

## Create a Portal

1. On the yawave home page, click **Portals > Create Portals**
2. [Complete the portal form](../Publish/Portals.md). 

**Note**: As you complete the portal form, make sure to select the category and sub-categories you configured for your new publication. Once you publish your new publication, it will then automatically get pulled into the portal.

## Apply Branding

1. From the yawave task menu, select **Administrate**.
2. Click **Brand** or **Theme** to apply [branding](../Administrate/Branding/Branding.md) and [themes](../Administrate/Branding/Theme.md) to your publication and portal. ![Branding](../assets/images/brand.jpg)

See also [About Branding and Themes](../Administrate/Branding/About-Branding-and-Themes.md).

## Configure Compliance Settings

1. From the yawave task menu, select **Administrate**.
2. Click **Settings > Compliance** to [add the required compliance settings for your publication](../Administrate/Settings/CompSettings.md).![Compliance](../assets/images/compliance.jpg)

## Publish a Publication

Return to your publication draft, and click **Publish**.

If you selected the same category for your publication as for your portal, your publication automatically appears in the portal you created.
