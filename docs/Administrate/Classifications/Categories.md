# Categories

Create categories to classify publication interest areas. Add sub-categories to narrow the interest focus. For example, if your publication category is Sports, you can add Soccer as as a subcategory. 

When you create a publication, you specify the category and sub-category to which that publication belongs.  

To ensure that your site remains dynamic and current, you can create portals to automatically pull in any publication with the categories and sub-categories you specified when you created the portal.

See also:

- [Build a Publication](../../Publish/PubBuilder/BuildPub.md)

- [Portals](../../Publish/Portals.md)

## Categories Dashboard Overview

From task menu, select **Administrate > Classify > Categories**.

The **Categories** dashboard opens, indicating the categories and sub-categories already created, as well as their usage and attributes. 

To view additional data such as organizations and interests, click the arrows at the far right of the screen beside the category names.

![Category Dashboard](../../assets/images/category.jpg)

## Create a Category

1. To create a category, click **Create Category**.
![Create Category](../../assets/images/create_cat_2.jpg)
2. Enter the following information on the **Create Category** form:
3. Enter the category name.
4. *Optional* Toggle the **Use as Interest** feature on or off. 
5. *Optional* Select an existing icon image or upload an icon.
6. Enter a **slug**, if it is different than the one that appears by default once you enter the category name.
7. *Optional* From the **Attritube** drop-down list, select an attribute for the new category.  
1. Click **Create**.

The new  category appears on the dashboard, similar to the image below.
![NewCat Dashboard](../../assets/images/NewCat.jpg)

For more information on attributes, see[Attributes](Attributes.md).

### Create a Sub-Category

Once you create a category, you can add a sub-category to it.

1. On the dashboard under **Subcategories,** click **Manage** beside the new category name.
2. On the **Manage *Category name* Subcategories** dashboard, click **Create Subcategory**.
3. Enter the fields in the **Create New Subcategory** form. These are the same type of fields that you configured in the **Create Category** form.
1. Click **Create**.
