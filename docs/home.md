## Welcome to yawave documentation!

*This page can be set up as tiles, linking to top-level workflows for key yawave task areas, with each tile using illustrative graphics related to the task area.*

### Explore Yawave
<!-- theme: success -->
>💡 **Get started on the **[yawave dashboard](GetStarted/Overview.md).
Then learn how to navigate in yawave.

*include link to the Getting Started > Overview topic, plus illustrative interface graphics.*

![Dashboard](assets/images/dashboard_1.jpg)

### Create publications
<!-- theme: success -->
>💡 **Get started with the **[Publications Workflow](GetStarted/PublicationFlow.md).
Then follow the easy steps required to create your first publication.

*include link to the Publication Workflow topic, plus illustrative interface graphics.*

![CreatePub](assets/images/discover_create-2.jpg)

---

### Engage your viewers

<!-- theme: success -->

> 💡 **Get Started with the** [Engagement Workflow](GetStarted/EngageTools.md)
and use yawave's tools to grow viewer engagement with your site.

*link to Engage Tools topic, plus illustrative graphics*

![EngageTool](assets/images/engage_workflow-2.jpg)

---

### Deploy Your Portals

<!-- theme: success -->

> 💡 **Get started with the** [Portal Workflow](GetStarted/PortalFlow.md) and follow the quick steps you use to assemble your publications in a portal.

*include link to the Portal Workflow topic, plus illustrative interface graphics.*

![PortalFlow](assets/images/PortalWkflow.jpg)

---

### Use Administration Tools

<!-- theme: success -->

> 💡 **Get started with** [Admininistraton Tools and Settings](GetStarted/AdminTools.md) for a quick introduction to your administration tools, including classifying categories and client branding.

*include link to the Using Administration Tools and Settings Getting Started topic, plus illustrative interface graphics.*

![AdminTool](assets/images/Admin_cat.jpg)

---

![faqs-icon](https://img.icons8.com/cotton/64/000000/scroll--v1.png)

### Developer Documentation

*This would be graphically a separate category, to clearly differentiate end user and developer users (unless end users do not see this documentation section)*

<!-- theme: success -->

> 💡 Developer documentation and reference guides.
>
