# Dimensions

## Dimensions Dashboard Overview
*Includes editing dimensions, as well as adding additional values, if applicable, and accessing the Contacts dashboard.*

## Create a Dimension

Field | Description 
---------|----------
 Name | "Describtion Name" 
 Slug | "Enter value Slug"
 Type | "Choose Type"

#### Choose Type
- Text
- Number
- Values
### Operations on single Dimensions
  Operation | Description 
---------|----------
 Edit | ... 
 Delete | ...
#### Add Value
If you select a value in the Dropdown "Choose Type", you can click on the number "0" and you can add values.

  Operation | Description 
---------|----------
 Name | "Enter value"
 Slug | "Enter value slug"