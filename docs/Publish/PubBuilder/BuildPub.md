# Build a Publication

Overview
*Describe how the publication builder is used in different workflows throughout the product (e.g. creating publications/pages/emails/templates, etc.) -> link to topics*

## Add Rows

Describe/show the type of rows users can add.

## Add Content Types
Users can add a variety of content types to each row of a publication.

Include a table that provides a snapshot and some details about each content type:
- Title
- Text
- Image
- Button
- Divider
- Social
- Dynamic Content
- HTML
- Video
- Icons
- Menu 


