# About the Publication Builder

Introduces the publication builder, and links to individual topics.

*This overview topic is useful for when you want to link from other topics in the document, for example from the Getting Started workflow topics.*

